%FITCARREAU Fits the Carreau 1972 parameters to data
%
%   -----
%   Input
%   -----
%   SHEARRATE  - Shear rate data from experiments
%   VISCDATA   - Viscosity data from experiments
%   LOWERBOUND - Lower bound for parameter fit
%   UPPERBOUND - Upper bound for parameter fit
%   INIT       - Initial conditions for parameter fit
%                   [mu0,muInf,lambda,n,a] where a is the Yasuda index              
%
%   ------
%   Output
%   ------
%   MU0     - Zero shear rate viscosity parameter
%   MUINF   - Infinite shear rate viscosity parameter
%   LAMBDA  - Relaxation time constant
%   N       - Power index
%	CYFIT   - CY fit to data
%
%   ---------------
%   Child functions
%   ---------------   
%   NONE
%
% Author: M.T.Gallagher, all rights reserved 2018
% E-mail: m.t.gallagher@bham.ac.uk
% URL:    http://www.meuriggallagher.com/
function [n,CFit,fval] = FitCarreau_Fixed_mu0_lambda(mu0,lambda,shearRate,viscData, ...
    bl,bu,init,varargin)

% Cost function
% c = [n]
constFunc = @(c) sum( (   mu0./ (1+(lambda*shearRate).^2).^(1/2 - c(1)/2) - viscData).^2);

% Solve using fmincon
options = optimoptions('fmincon','Display','none');
[fittedParams,fval] = fmincon(constFunc,init,[],[],[],[], ...
    bl,bu,[],options);

% Output
n = fittedParams(1);

CFit = @(g) mu0 ./ ( (1 + (lambda*g).^2).^(1/2-n/2) );

if isempty(varargin)
    fprintf('Carreau-1972 alternative fit:\n')
    fprintf('\t mu0 = %f\n',mu0)
    fprintf('\t lambda = %f\n',lambda)
    fprintf('\t n = %f\n',n)
    fprintf('\t fval = %f\n',fval);
end
