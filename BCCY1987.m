function mu=BCCY1987(params,gam)

mu0=params.mu0;
muinf=params.muinf;
lambda=params.lambda;
n=params.n;
a=params.a;

mu=muinf+(mu0-muinf)./(1+(lambda*gam).^a).^((1-n)/a);
