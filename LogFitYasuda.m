%LOGFITYASUDA Fits the Yasuda-1979 parameters to data, log both first
%
%   -----
%   Input
%   -----
%   SHEARRATE  - Shear rate data from experiments
%   VISCDATA   - Viscosity data from experiments
%   LOWERBOUND - Lower bound for parameter fit
%   UPPERBOUND - Upper bound for parameter fit
%   INIT       - Initial conditions for parameter fit
%                   [mu0,muInf,lambda,n,a] where a is the Yasuda index              
%
%   ------
%   Output
%   ------
%   MU0     - Zero shear rate viscosity parameter
%   MUINF   - Infinite shear rate viscosity parameter
%   LAMBDA  - Relaxation time constant
%   N       - Power index
%	CYFIT   - CY fit to data
%
%   ---------------
%   Child functions
%   ---------------   
%   NONE
%
% Author: M.T.Gallagher, all rights reserved 2018
% E-mail: m.t.gallagher@bham.ac.uk
% URL:    http://www.meuriggallagher.com/
function [mu0,lambda,n,a,CFit,fval,hessian] = LogFitYasuda(shearRate, ...
    viscData,bl,bu,init,varargin)

% Cost function
% c = [mu_0, lambda, n, a]
constFunc = @(c) sum( (  log(c(1)./ (1+(c(2)*shearRate).^c(4)).^(1/c(4) - c(3)/c(4))) ...
                       - log(viscData)).^2);

% Solve using fmincon
options = optimoptions('fmincon','Display','none');
[fittedParams,~] = fmincon(constFunc,init,[],[],[],[], ...
    bl,bu,[],options);

% refit with fminunc to get hessian
options = optimoptions('fminunc','Display','none');
[fittedParams, fval,~,~,~,hessian] = fminunc(constFunc, ...
    fittedParams,options);

% Output
mu0 = fittedParams(1);
lambda = fittedParams(2);
n = fittedParams(3);
a = fittedParams(4);

CFit = @(g) mu0 ./  (1 + (lambda*g).^a).^(1/a-n/a) ;

if isempty(varargin)
    fprintf('Yasuda-1979 fit:\n')
    fprintf('\t mu0 = %f\n',mu0)
    fprintf('\t lambda = %f\n',lambda)
    fprintf('\t n = %f\n',n)
    fprintf('\t a = %f\n',a)
    fprintf('\t fval = %f\n',fval);
end

end

