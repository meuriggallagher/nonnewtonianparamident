function [gammadot] = CalcShearRate(mu,G,r)

gammadot=0*r;

options = optimoptions('fsolve','Display','none');
for m=1:length(r)
    residualFn  = @(x) G*r(m)/2-mu.fn(x,mu.params)*x;
    gammadot(m) = fsolve(residualFn,0,options);
end

