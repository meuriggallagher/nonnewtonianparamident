%LOGFITBCY Fits the Bird-Carreau-Yasuda 1987 parameters to data, log both first
%
%   -----
%   Input
%   -----
%   SHEARRATE  - Shear rate data from experiments
%   VISCDATA   - Viscosity data from experiments
%   LOWERBOUND - Lower bound for parameter fit
%   UPPERBOUND - Upper bound for parameter fit
%   INIT       - Initial conditions for parameter fit
%                   [mu0,muInf,lambda,n,a] where a is the Yasuda index              
%
%   ------
%   Output
%   ------
%   MU0     - Zero shear rate viscosity parameter
%   MUINF   - Infinite shear rate viscosity parameter
%   LAMBDA  - Relaxation time constant
%   N       - Power index
%	CYFIT   - CY fit to data
%
%   ---------------
%   Child functions
%   ---------------   
%   NONE
%
% Author: M.T.Gallagher, all rights reserved 2018
% E-mail: m.t.gallagher@bham.ac.uk
% URL:    http://www.meuriggallagher.com/
function [mu0,muinf,lambda,n,a,CFit,fval] = LogFitBCCY(shearRate, ...
    viscData,bl,bu,init,varargin)

% Cost function
% c = [mu_0, mu_inf, lambda, n, a]
constFunc = @(c) sum( (  log( c(2) +  (c(1)-c(2))./ ...
    (1+(c(3)*shearRate).^c(5)).^(1/c(5) - c(4)/c(5))) ...
   - log(viscData)).^2);

% Solve using fmincon
options = optimoptions('fmincon','Display','none');
[fittedParams,fval] = fmincon(constFunc,init,[],[],[],[], ...
    bl,bu,[],options);

% Output
mu0 = fittedParams(1);
muinf = fittedParams(2);
lambda = fittedParams(3);
n = fittedParams(4);
a = fittedParams(5);

CFit = @(g) muinf + (mu0-muinf) ./  (1 + (lambda*g).^a).^(1/a-n/a) ;

if isempty(varargin)
    fprintf('BCCY-1984 fit:\n')
    fprintf('\t mu0 = %f\n',mu0)
    fprintf('\t muinf = %f\n',muinf)
    fprintf('\t lambda = %f\n',lambda)
    fprintf('\t n = %f\n',n)
    fprintf('\t a = %f\n',a)
    fprintf('\t fval = %f\n',fval)
end

end