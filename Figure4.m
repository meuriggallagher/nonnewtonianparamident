%% Cross Zero
% Bounds
%     mu0, lambda, n
bl = [50;  1;     0.01];
bu = [350; 200;   1];
x0 = [200,  100,   0.5];

% Solve
[logCrossZeroModel.mu0,logCrossZeroModel.lambda, ...
    logCrossZeroModel.n,logCrossZeroModel.fit,logCrossZeroModel.fval] = LogFitCrossZero( ...
    SkalakData(:,1),SkalakData(:,2),bl,bu,x0);

%% CrossZero. Fix each (mu0,lambda) in turn and find optimal n 
%   and associated fnval via logfit
% mu0, lambda
bl = 0.01;
bu = 1;
x0 = 0.5;

mu0=linspace(50,350,40);
lambda=linspace(40,200,45);

fprintf('Fixing (mu0,lambda) pairs, fitting n, calculating fval ... ')
for j=1:length(mu0)
    for k=1:length(lambda)
        [n,fit,fval]=LogFitCrossZero_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),SkalakData(:,1),SkalakData(:,2),bl,bu,x0,0);
        logCrossZeroModelArray.n(j,k)=n;
        logCrossZeroModelArray.fit{j,k}=fit;
        logCrossZeroModelArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 4a
figure(4);
clf;
subplot(2,1,1)
hold on
wd=12.5;hh=5.2;
hc=contour(lambda,mu0,logCrossZeroModelArray.fval,100);colorbar;
plot(logCrossZeroModel.lambda,logCrossZeroModel.mu0,'bd');
%plot(params.lambda,params.mu0,'rx');
hx=xlabel('\(\lambda\)','interpreter','latex');
hy=ylabel('\(\mu_0\)','interpreter','latex');
ht=title('\(\ell(\mu_0,\lambda,\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');
set(4,'paperunits','centimeters');
set(4,'papersize',[wd hh]);
set(4,'paperposition',[0 0 wd hh]);

%% Figure 4b
subplot(2,2,3);
plot(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
plot(gam,logCrossZeroModel.fit(gam),'b');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%% Figure 4c
subplot(2,2,4)
loglog(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
loglog(gam,logCrossZeroModel.fit(gam),'b');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%%
print(4,'-dpdf','figure4.pdf')