%% Yasuda-1979 first fit
% Bounds
%    mu0   lambda n     a
bl = [1,    1,    0.01, 0.01];
bu = [350,  200,  10,   5];
x0 = [100,  100,  0.5 , 2];

%solve
[logYasudaModel.mu0,logYasudaModel.lambda,...
    logYasudaModel.n,logYasudaModel.a,logYasudaModel.fit,logYasudaModel.fval] = ... 
    LogFitYasuda(SkalakData(:,1),SkalakData(:,2),bl,bu,x0);

%% Yasuda - Fix each (mu0,lambda) in turn and find optimal n,a 
%   and associated fnval via logfit
% n, a
bl = [0.01, 0.1];
bu = [1,     5];
x0 = [0.5,   2];

mu0=linspace(50,350,90);
lambda=linspace(40,200,120);

fprintf('Fixing (mu0,lambda) pairs, fitting n,a, calculating fval ... ')
for j=1:length(mu0)
    for k=1:length(lambda)
        [n,a,fit,fval]=LogFitYasuda_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),SkalakData(:,1),SkalakData(:,2),bl,bu,x0,0);
        logYasudaModelArray.n(j,k)=n;
        logYasudaModelArray.a(j,k)=a;
        logYasudaModelArray.fit{j,k}=fit;
        logYasudaModelArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 6a
figure(6);
clf;
subplot(2,1,1);
hold on;
wd=12.5;hh=5.2;
hc=contour(lambda,mu0,logYasudaModelArray.fval,100);colorbar;
plot(logYasudaModel.lambda,logYasudaModel.mu0,'bd');
hx=xlabel('\(\lambda\)','interpreter','latex');
hy=ylabel('\(\mu_0\)','interpreter','latex');
ht=title('\(\ell(\mu_0,\lambda,\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');
set(6,'paperunits','centimeters');
set(6,'papersize',[wd hh]);
set(6,'paperposition',[0 0 wd hh]);

%% Figure 6b
subplot(2,2,3)
plot(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
plot(gam,logYasudaModel.fit(gam),'b');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%% Figure 6c
subplot(2,2,4)
loglog(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
loglog(gam,logYasudaModel.fit(gam),'b');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%%
print(6,'-dpdf','figure6.pdf')
