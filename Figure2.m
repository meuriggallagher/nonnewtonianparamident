%% Carreau 1972 log fit
% mu0, lambda, n
bl = [1,1,  0.01];
bu = [350,200,1];
x0 = [240,100,0.5];

%solve
[logCarreauModel.mu0,logCarreauModel.lambda,...
    logCarreauModel.n,logCarreauModel.fit,logCarreauModel.fval, ...
    logCarreauModel.hessian] = LogFitCarreau1972(SkalakData(:,1), ...
    SkalakData(:,2),bl,bu,x0);

%% Carreau 1972 alternative log fit
bl = 0.001;
bu = 1;
x0 = 0.44;
params.mu0=107;
params.lambda=100;
[params.n,fit,fval]=LogFitCarreau_Fixed_mu0_lambda(params.mu0, ...
    params.lambda,SkalakData(:,1),SkalakData(:,2),bl,bu,x0);

%% Carreau-1972. Fix each (mu0,lambda) in turn and find optimal n and associated fnval via logfit
% n
bl = 0.001;
bu = 1;
x0 = 0.44;

mu0=linspace(50,350,40);
lambda=linspace(40,200,45);

fprintf('Fixing (mu0,lambda) pairs, fitting n, calculating fval ... ')
for j=1:length(mu0)
    for k=1:length(lambda)
        [n,fit,fval]=LogFitCarreau_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),SkalakData(:,1),SkalakData(:,2),bl,bu,x0,0);
        logCarreauModelArray.n(j,k)=n;
        logCarreauModelArray.fit{j,k}=fit;
        logCarreauModelArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 2a
figure(2);
clf;
subplot(2,1,1);
hold on;
wd=12.5;hh=5.2;
hc=contour(lambda,mu0,logCarreauModelArray.fval,100);colorbar;
plot(logCarreauModel.lambda,logCarreauModel.mu0,'bd');
plot(params.lambda,params.mu0,'rx');
hx=xlabel('\(\lambda\)','interpreter','latex');
hy=ylabel('\(\mu_0\)','interpreter','latex');
ht=title('\(\ell(\mu_0,\lambda,\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');
set(2,'paperunits','centimeters');
set(2,'papersize',[wd hh]);
set(2,'paperposition',[0 0 wd hh]);

%% Figure 2b
subplot(2,2,3);
plot(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
plot(gam,logCarreauModel.fit(gam),'b');
plot(gam,Carreau1972(params,gam),'r--');
hl=legend('Skalak data','best fit','alternative fit');
set(hl,'box','off');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%% Figure 2c
subplot(2,2,4);
loglog(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
loglog(gam,logCarreauModel.fit(gam),'b');
loglog(gam,Carreau1972(params,gam),'r--');
hl=legend('Skalak data','best fit','alternative fit');
set(hl,'box','off');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%%
print(2,'-dpdf','figure2.pdf');
