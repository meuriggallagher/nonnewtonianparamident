%LOGFITCARREAU Fits the Carreau 1972 parameters to data, log both first
%
%   -----
%   Input
%   -----
%   SHEARRATE  - Shear rate data from experiments
%   VISCDATA   - Viscosity data from experiments
%   LOWERBOUND - Lower bound for parameter fit
%   UPPERBOUND - Upper bound for parameter fit
%   INIT       - Initial conditions for parameter fit
%                   [mu0,muInf,lambda,n,a] where a is the Yasuda index              
%
%   ------
%   Output
%   ------
%   MU0     - Zero shear rate viscosity parameter
%   MUINF   - Infinite shear rate viscosity parameter
%   LAMBDA  - Relaxation time constant
%   N       - Power index
%	CYFIT   - CY fit to data
%
%   ---------------
%   Child functions
%   ---------------   
%   NONE
%
% Author: M.T.Gallagher, all rights reserved 2018
% E-mail: m.t.gallagher@bham.ac.uk
% URL:    http://www.meuriggallagher.com/
function [mu0,lambda,n,CFit,fval,hessian] = LogFitCarreau1972(shearRate, ...
    viscData,bl,bu,init)

% Cost function
% c = [mu_0, lambda, n]

constFunc = @(c) sum( (  log(c(1)./ (1+(c(2)*shearRate).^2).^(1/2 - c(3)/2)) ...
                       - log(viscData)).^2);

% Solve using fmincon
options = optimoptions('fmincon','Display','none');
[fittedParams,~] = fmincon(constFunc,init,[],[],[],[], ...
    bl,bu,[],options);

% refit with fminunc to get hessian
options = optimoptions('fminunc','Display','none');
[fittedParams, fval,~,~,~,hessian] = fminunc(constFunc,fittedParams,options);

% Output
mu0 = fittedParams(1);
lambda = fittedParams(2);
n = fittedParams(3);

CFit = @(g) mu0 ./  (1 + (lambda*g).^2).^(1/2-n/2) ;

fprintf('Carreau 1972 log fit:\n')
fprintf('\t mu0 = %f\n',mu0)
fprintf('\t lambda = %f\n',lambda)
fprintf('\t n = %f\n',n)
fprintf('\t fval = %f\n',fval);