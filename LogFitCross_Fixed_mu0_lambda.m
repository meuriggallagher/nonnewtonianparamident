% Fits the Cross-1965 parameters to data, fixed mu0 and lambda
%
%   -----
%   Input
%   -----
%   SHEARRATE  - Shear rate data from experiments
%   VISCDATA   - Viscosity data from experiments
%   LOWERBOUND - Lower bound for parameter fit
%   UPPERBOUND - Upper bound for parameter fit
%   INIT       - Initial conditions for parameter fit
%                   [mu0,muInf,lambda,n,a] where a is the Yasuda index
%
%   ------
%   Output
%   ------
%   MU0     - Zero shear rate viscosity parameter
%   MUINF   - Infinite shear rate viscosity parameter
%   LAMBDA  - Relaxation time constant
%   N       - Power index
%	CYFIT   - CY fit to data
%
%   ---------------
%   Child functions
%   ---------------
%   NONE
%
% Author: M.T.Gallagher, all rights reserved 2018
% E-mail: m.t.gallagher@bham.ac.uk
% URL:    http://www.meuriggallagher.com/
function [muinf,n,CrFit,fval] = LogFitCross_Fixed_mu0_lambda(mu0,lambda, ...
    shearRate,viscData,bl,bu,init,varargin)

% Cost function
% c = [muinf,n]
constFunc = @(c) sum(( log( c(1) + (mu0-c(1))./(1+(lambda*shearRate).^(1-c(2)) )) ...
                      -log(viscData)   ).^2);

% Solve using fmincon
options = optimoptions('fmincon','Display','none');
[fittedParams, fval] = fmincon(constFunc,init,[],[],[],[], ...
    bl,bu,[],options);

% Output
muinf = fittedParams(1);
n = fittedParams(2);

CrFit = @(x) muinf + (mu0-muinf) ./ ( 1 + (lambda*x).^(1-n) );

if isempty(varargin)
    fprintf('Cross-1965 alternative fit:\n')
    fprintf('\t mu0 = %f\n',mu0)
    fprintf('\t muInf = %f\n',muinf)
    fprintf('\t lambda = %f\n',lambda)
    fprintf('\t n = %f\n',n)
    fprintf('\t fval = %f\n',fval);
end

end