%% Carreau 1972 first fit
% Bounds
% mu0, lambda, n
bl = [1,1,  0.01];
bu = [350,200,1];
x0 = [240,100,0.5];

%solve
[carreauModel.mu0,carreauModel.lambda,...
    carreauModel.n,carreauModel.fit,carreauModel.fval,carreauModel.hessian] = ... 
    FitCarreau1972(SkalakData(:,1),SkalakData(:,2),bl,bu,x0);

%% Carreau 1972 alternative fit
bl=0.01;
bu=1;
n0=0.5;

params_alt.mu0=211;
params_alt.lambda=100;
[params_alt.n,fit_alt,params_alt.fval_alt]=FitCarreau_Fixed_mu0_lambda(params_alt.mu0,params_alt.lambda,SkalakData(:,1),SkalakData(:,2),bl,bu,n0);

%% Carreau-1972. Fix each (mu0,lambda) in turn and find optimal n and associated fnval
% n
bl = 0.001;
bu = 1;
x0 = 0.44;

mu0=linspace(50,350,40);
lambda=linspace(40,200,45);

clear carreauModelArray;

fprintf('Fixing (mu0,lambda) pairs, fitting n, calculating fval ... ')
for j=1:length(mu0)
    for k=1:length(lambda)
        [n,fit,fval]=FitCarreau_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),SkalakData(:,1),SkalakData(:,2),bl,bu,x0,0);
        carreauModelArray.n(j,k)=n;
        carreauModelArray.fit{j,k}=fit;
        carreauModelArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 1a
figure(1);
clf;
subplot(2,1,1);
hold on;
wd=12.5;hh=5.2;
hc=contour(lambda,mu0,carreauModelArray.fval,200);colorbar;
plot(carreauModel.lambda,carreauModel.mu0,'b*');
plot(params_alt.lambda,params_alt.mu0,'r+');
hx=xlabel('\(\lambda\)','interpreter','latex');
hy=ylabel('\(\mu_0\)','interpreter','latex');
ht=title('\(\mathcal{L}(\mu_0,\lambda,*)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');
set(1,'paperunits','centimeters');
set(1,'papersize',[wd hh]);
set(1,'paperposition',[0 0 wd hh]);


%% Figure 1b
subplot(2,2,3)
plot(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
plot(gam,carreauModel.fit(gam),'b');
plot(gam,Carreau1972(params_alt,gam),'r--');
hl=legend('Skalak data','best fit','alternative fit');
set(hl,'box','off');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%% Figure 1c
subplot(2,2,4)
loglog(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
loglog(gam,carreauModel.fit(gam),'b');
loglog(gam,Carreau1972(params_alt,gam),'r--');
hl=legend('Skalak data','best fit','alternative fit');
set(hl,'box','off');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%%
print(1,'-dpdf','figure1.pdf');
