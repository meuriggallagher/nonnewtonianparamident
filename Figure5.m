%% Cross-1965 first fit
% Bounds
%     mu0, mu_inf, lambda, n
bl = [50;  0.01 ;  1;     0.1];
bu = [350; 50   ;  200;   1];
x0 = [200,  3   ;  100,   0.5];

% Solve
[logCrossModel.mu0,logCrossModel.muinf,logCrossModel.lambda, ...
    logCrossModel.n,logCrossModel.fit,logCrossModel.fval] = LogFitCross( ...
    SkalakData(:,1),SkalakData(:,2),bl,bu,x0);

%% Cross-1965. Fix each (mu0,lambda) in turn and find
%   optimal (muinf,n) and associated fnval via logfit
% muinf, n
bl = [0.01 ;0.1];
bu = [20   ;1  ];
x0 = [3    ;0.5];

mu0=linspace(50,350,40);
lambda=linspace(40,200,45);

fprintf('Fixing (mu0,lambda) pairs, fitting n, calculating fval ... ')
for j=1:length(mu0)    
    for k=1:length(lambda)
        [muinf,n,fit,fval]=LogFitCross_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),SkalakData(:,1),SkalakData(:,2),bl,bu,x0,0);
        logCrossModelArray.muinf(j,k)=muinf;
        logCrossModelArray.n(j,k)=n;
        logCrossModelArray.fit{j,k}=fit;
        logCrossModelArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 5a
figure(5);
clf;
subplot(2,1,1);
hold on;
wd=12.5;hh=5.2;
hc=contour(lambda,mu0,logCrossModelArray.fval,100);colorbar;
plot(logCrossModel.lambda,logCrossModel.mu0,'bd');
hx=xlabel('\(\lambda\)','interpreter','latex');
hy=ylabel('\(\mu_0\)','interpreter','latex');
ht=title('\(\ell(\mu_0,\lambda;\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');
set(5,'paperunits','centimeters');
set(5,'papersize',[wd hh]);
set(5,'paperposition',[0 0 wd hh]);

%% Figure 5b
subplot(2,2,3)
plot(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
plot(gam,logCrossModel.fit(gam),'b');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%% Figure 5c
subplot(2,2,4)
loglog(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
loglog(gam,logCrossModel.fit(gam),'b');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%%
print(5,'-dpdf','figure5.pdf')