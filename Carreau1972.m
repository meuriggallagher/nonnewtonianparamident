function mu=Carreau1972(params,gam)

mu0=params.mu0;
lambda=params.lambda;
n=params.n;

mu=mu0./(1+(lambda*gam).^2).^((1-n)/2);