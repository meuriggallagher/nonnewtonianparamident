function u=CalcVelField(mu,G,r)

% r is a vector of radius values from 0 to rmax

gammadot=CalcShearRate(mu,G,r);

u=0*r;
dr=r(2)-r(1);
M=length(r);
% trapezium rule integral of gammadot = du/dr
for m=1:M-1
    u(m)=dr*0.5*sum(gammadot(m:M-1)+gammadot(m+1:M));
end
