%FITCARREAU Fits the Carreau 1972 parameters to data
%
%   -----
%   Input
%   -----
%   SHEARRATE  - Shear rate data from experiments
%   VISCDATA   - Viscosity data from experiments
%   LOWERBOUND - Lower bound for parameter fit
%   UPPERBOUND - Upper bound for parameter fit
%   INIT       - Initial conditions for parameter fit
%                   [mu0,muInf,lambda,n,a] where a is the Yasuda index              
%
%   ------
%   Output
%   ------
%   MU0     - Zero shear rate viscosity parameter
%   MUINF   - Infinite shear rate viscosity parameter
%   LAMBDA  - Relaxation time constant
%   N       - Power index
%	CYFIT   - CY fit to data
%
%   ---------------
%   Child functions
%   ---------------   
%   NONE
%
% Author: M.T.Gallagher, all rights reserved 2018
% E-mail: m.t.gallagher@bham.ac.uk
% URL:    http://www.meuriggallagher.com/
function [tau0,muPl,CFit,fval,hessian] = FitCasson(shearRate,viscData, ...
    bl,bu,init)

% Cost function
% c = [tau0, muPl]
% viscData = viscData.^0.5;
% shearRate = shearRate.^0.5;
% costFunc = @(c) sum( ((c(1)./shearRate + c(2)) - viscData).^2);
costFunc = @(c) sum( (sqrt(c(1)./shearRate) + sqrt(c(2)) - sqrt(viscData)).^2);

% Solve using fmincon
[fittedParams,~] = fmincon(costFunc,init,[],[],[],[], ...
    bl,bu);

% refit with fminunc to get hessian
[fittedParams, fval,~,~,~,hessian] = fminunc(costFunc,fittedParams);

% Output
tau0 = fittedParams(1);
muPl = fittedParams(2);

CFit = @(g) (sqrt(tau0./g) + muPl^0.5).^2;

fprintf('Casson fit:\n')
fprintf('\t tau0 = %f\n',tau0)
fprintf('\t muPl = %f\n',muPl)
