# NonNewtonianParamIdent

# This repository contains all the data and code for the paper:
#  Non-identifiability of parameters for a class of shear-thinning rheological models, with implications for haematological fluid dynamics
#   Meurig T. Gallagher, Richard A. J. Wain, Sonia Dari, Justin P. Whitty, David J. Smith
#
# https://arxiv.org/abs/1810.02292