%% Velocity plots

%% Pipe flow, Carreau, r = 0.1cm
G=10;
L=0.1;

r=linspace(0,L,50);
mu1.fn=@(x,params) Carreau3Param(x,params);
mu1.params.mu0=301;
mu1.params.lambda=200;
mu1.params.n=0.483;

mu2.fn=@(x,params) Carreau3Param(x,params);
mu2.params.mu0=211;
mu2.params.lambda=100;
mu2.params.n=0.482;

u1=CalcVelField(mu1,G,r);
u2=CalcVelField(mu2,G,r);

%% Figure 8a
wd=7.5;hh=7;
figure(8);
clf;
subplot(2,2,1)
hold on;
plot(u1,r,'b');
plot(u2,r,'r--');
box on;
xlabel('\(u\) (cm/s)','interpreter','latex');
ylabel('\(r\) (cm)','interpreter','latex');
hl = legend('best fit','alt. fit');
set(hl,'location','southwest');
set(hl,'box','off');
set(8,'paperunits','centimeters');
set(8,'papersize',[wd hh]);
set(8,'paperposition',[0 0 wd hh]);

%% Pipe flow, Carreau, r = 1cm
G=10;
L=1;

r=linspace(0,L,50);
mu1.fn=@(x,params) Carreau3Param(x,params);
mu1.params.mu0=301;
mu1.params.lambda=200;
mu1.params.n=0.483;

mu2.fn=@(x,params) Carreau3Param(x,params);
mu2.params.mu0=211;
mu2.params.lambda=100;
mu2.params.n=0.482;

u1=CalcVelField(mu1,G,r);
u2=CalcVelField(mu2,G,r);

%% Figure 8b
subplot(2,2,2)
hold on;
plot(u1,r,'b');
plot(u2,r,'r--');
box on;
xlabel('\(u\) (cm/s)','interpreter','latex');
ylabel('\(r\) (cm)','interpreter','latex');
hl=legend('best fit','alt. fit');
set(hl,'location','southwest');
set(hl,'box','off');

%% Pipe flow, BCCY, r = 0.1cm
G=10;
L=0.1;

r=linspace(0,L,50);
mu1.fn=@(x,params) BCCY5Param(x,params);
mu1.params.mu0=89.8;
mu1.params.muinf=3.03;
mu1.params.lambda=14.2;
mu1.params.n=0.339;
mu1.params.a=2.15;

mu2.fn=@(x,params) BCCY5Param(x,params);
mu2.params.mu0=350;
mu2.params.muinf=3.03;
mu2.params.lambda=107;
mu2.params.n=0.334;
mu2.params.a=0.755;

u1=CalcVelField(mu1,G,r);
u2=CalcVelField(mu2,G,r);

%% Figure 8c
subplot(2,2,3);
hold on;
plot(u1,r,'b');
plot(u2,r,'r--');
box on;
set(gca,'tickdir','out');
xlabel('\(u\) (cm/s)','interpreter','latex');
ylabel('\(r\) (cm)','interpreter','latex');
hl=legend('best fit','alt. fit');
set(hl,'location','southwest');
set(hl,'box','off');

%% Pipe flow, BCCY, r = 1cm
G=10;
L=1;

r=linspace(0,L,50);
mu1.fn=@(x,params) BCCY5Param(x,params);
mu1.params.mu0=89.8;
mu1.params.muinf=3.03;
mu1.params.lambda=14.2;
mu1.params.n=0.339;
mu1.params.a=2.15;

mu2.fn=@(x,params) BCCY5Param(x,params);
mu2.params.mu0=350;
mu2.params.muinf=3.03;
mu2.params.lambda=107;
mu2.params.n=0.334;
mu2.params.a=0.755;

u1=CalcVelField(mu1,G,r);
u2=CalcVelField(mu2,G,r);

%% Figure 8d
subplot(2,2,4);
hold on;
plot(u1,r,'b');
plot(u2,r,'r--');
box on;
xlabel('\(u\) (cm/s)','interpreter','latex');
ylabel('\(r\) (cm)','interpreter','latex');
hl=legend('best fit','alt. fit');
set(hl,'location','southwest');
set(hl,'box','off');

%%
print(8,'-dpdf','figure8.pdf')
