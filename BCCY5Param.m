function mu=BCCY5Param(gammadot,params)

    mu=params.muinf+(params.mu0-params.muinf)/(1+(params.lambda*gammadot)^params.a).^((1-params.n)/params.a);

end