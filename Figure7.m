%% BCCY-1987 first fit
%    mu0   muinf   lambda  n     a
bl = [1,    0.01,   1,     0.01, 0.01];
bu = [350,  50,     200,   10,   5];
x0 = [100,  3,      100,   0.5 , 2];

%solve
[logBCCYModel.mu0,logBCCYModel.muinf,logBCCYModel.lambda,...
    logBCCYModel.n,logBCCYModel.a,logBCCYModel.fit,logBCCYModel.fval] = ... 
    LogFitBCCY(SkalakData(:,1),SkalakData(:,2),bl,bu,x0);

%% BCCY-1987 alternative fit
% muinf, n, a
bl = [0.01, 0.01,  0.01];
bu = [50,   1,     5];
x0 = [3,    0.5,   2];

params_alt.mu0=350;
params_alt.lambda=107.26;
[params_alt.muinf,params_alt.n,params_alt.a,fit,params_alt.fval_alt] ...
    = LogFitBCCY_Fixed_mu0_lambda(params_alt.mu0, ...
    params_alt.lambda,SkalakData(:,1),SkalakData(:,2),bl,bu,x0);

%% BCCY-1987 - Fix each (mu0,lambda) in turn and find 
%   optimal muinf,n,a and associated fnval via logfit
% muinf, n, a
bl = [0.01, 0.01,  0.01];
bu = [50,   1,     5];
x0 = [3,    0.5,   2];

mu0=linspace(10,350,60);
lambda=linspace(1,150,65);

fprintf('Fixing (mu0,lambda) pairs, fitting muInf,n,a, calculating fval ... ')
for j=1:length(mu0)
    for k=1:length(lambda)
        [muinf,n,a,fit,fval]=LogFitBCCY_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),SkalakData(:,1),SkalakData(:,2),bl,bu,x0,0);
        logBCYModelArray.muinf(j,k)=muinf;
        logBCYModelArray.n(j,k)=n;
        logBCYModelArray.a(j,k)=a;
        logBCYModelArray.fit{j,k}=fit;
        logBCYModelArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 7a
wd=12.5;hh=5.2;
figure(7);
clf;
subplot(2,1,1);
hold on;
hc=contour(lambda,mu0,logBCYModelArray.fval,100);colorbar;
plot(logBCCYModel.lambda,logBCCYModel.mu0,'bd');
plot(params_alt.lambda,params_alt.mu0,'rx');
xlabel('\(\lambda\)','interpreter','latex');
ylabel('\(\mu_0\)','interpreter','latex');
title('\(\ell(\mu_0,\lambda,\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');
set(7,'paperunits','centimeters');
set(7,'papersize',[wd hh]);
set(7,'paperposition',[0 0 wd hh]);

%% Figure 7b
subplot(2,2,3);
plot(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
plot(gam,logBCCYModel.fit(gam),'b');
plot(gam,BCCY1987(params_alt,gam),'r--');
hl=legend('Skalak data','best fit','alternative fit');
set(hl,'box','off');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%% Figure 7c
subplot(2,2,4)
loglog(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),200);
loglog(gam,logBCCYModel.fit(gam),'b');
loglog(gam,BCCY1987(params_alt,gam),'r--');
hl=legend('Skalak data','best fit','alternative fit');
set(hl,'box','off');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
box on;

%%
print(7,'-dpdf','figure7.pdf')