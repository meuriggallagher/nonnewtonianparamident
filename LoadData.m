%% Load data

load('Cokelet1963.mat','Cokelet1963')
load('Merrill1963.mat','Merrill1963')
load('Huang1973.mat','Huang1973')
load('Salak1981.mat','Skalak1981')

DataSet = 10.^[Merrill1963; Cokelet1963; Huang1973; Skalak1981];


MerrillData = 10.^Merrill1963;
CokeletData = 10.^Cokelet1963;
HuangData = 10.^Huang1973;
SkalakData = 10.^Skalak1981;

%% Comparison
figure(10);clf;
loglog(10.^Cokelet1963(:,1),10.^Cokelet1963(:,2),'xb','markersize',8)
hold on

loglog(10.^Merrill1963(:,1),10.^Merrill1963(:,2),'xr','markersize',8)

loglog(10.^Huang1973(:,1),10.^Huang1973(:,2),'xk','markersize',8)

loglog(10.^Skalak1981(:,1),10.^Skalak1981(:,2),'x','color',[0,0.5,0],'markersize',8)

hold off
title('Experimental data sets')
legend('Cokelet 1963', 'Merrill 1963', 'Huang 1973', 'Skalak 1981')
