function mu=Carreau3Param(gammadot,params)

    mu=params.mu0./(1+(params.lambda*gammadot).^2).^((1-params.n)/2);

end