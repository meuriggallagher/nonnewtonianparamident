%% Appendix: Check other data sets with Carreau-1972

% Carreau-1972. Fix each (mu0,lambda) in turn and find optimal n and associated fnval via logfit
% mu0, lambda
bl = 0.001;
bu = 1;
x0 = 0.44;

mu0=linspace(50,350,40);
lambda=linspace(40,200,45);

fprintf('Fitting to Merrill-1963 data set ...')
for j=1:length(mu0)
    for k=1:length(lambda)
        [n,fit,fval]=LogFitCarreau_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),MerrillData(:,1),MerrillData(:,2),bl,bu,x0,0);
        logCarreauModelArray.n(j,k)=n;
        logCarreauModelArray.fit{j,k}=fit;
        logCarreauModelArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 9a
figure(9);
clf;
subplot(2,2,1);
contour(lambda,mu0,logCarreauModelArray.fval,100);colorbar;
xlabel('\(\lambda\)','interpreter','latex');
ylabel('\(\mu_0\)','interpreter','latex');
title('\(\ell(\mu_0,\lambda,\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');
set(9,'paperunits','centimeters');
set(9,'papersize',[wd hh]);
set(9,'paperposition',[0 0 wd hh]);

%%
clearvars logCarreauModelArray;
fprintf('Fitting to Cokelet-1963 data set ...')
for j=1:length(mu0)
    for k=1:length(lambda)
        [n,fit,fval]=LogFitCarreau_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),CokeletData(:,1),CokeletData(:,2),bl,bu,x0,0);
        logCarreauModelArray.n(j,k)=n;
        logCarreauModelArray.fit{j,k}=fit;
        logCarreauModelArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 9b
subplot(2,2,2);
contour(lambda,mu0,logCarreauModelArray.fval,100);colorbar;
xlabel('\(\lambda\)','interpreter','latex');
ylabel('\(\mu_0\)','interpreter','latex');
title('\(\ell(\mu_0,\lambda,\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');

%%

clearvars logCarreauModelArray;
fprintf('Fitting to Huang-1973 data set ...')
for j=1:length(mu0)    
    for k=1:length(lambda)
        [n,fit,fval]=LogFitCarreau_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),HuangData(:,1),HuangData(:,2),bl,bu,x0,0);
        logCarreauModelArray.n(j,k)=n;
        logCarreauModelArray.fit{j,k}=fit;
        logCarreauModelArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 9c
subplot(2,1,2);
contour(lambda,mu0,logCarreauModelArray.fval,100);colorbar;
xlabel('\(\lambda\)','interpreter','latex');
ylabel('\(\mu_0\)','interpreter','latex');
title('\(\ell(\mu_0,\lambda,\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');

%%
print(9,'-dpdf','figure9.pdf')