%% figure 3: Carreau-1972 simulation study

% Define 'true' parameter set and add noise
trueParams.mu0=150;
trueParams.lambda=100;
trueParams.n=0.45;

nSamp=50;
sigma=0.02;
gamSynth=10.^linspace(-3,3,nSamp);
noise=normrnd(0,sigma,1,nSamp);
muSynth=Carreau1972(trueParams,gamSynth).*exp(-noise);

%% Figure 3a
figure(3);
clf;
wd=11;hh=6.5;
subplot(2,1,1)
loglog(gamSynth,muSynth,'o');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
xlim([0.001 1000]);
box on;
set(gca,'tickdir','out');
set(3,'paperunits','centimeters');
set(3,'papersize',[wd hh]);
set(3,'paperposition',[0 0 wd hh]);

%% Carreau-1972 log fnval fit for all synth data
bl = 0.001;
bu = 1;
x0 = 0.44;

mu0=linspace(50,350,40);
lambda=linspace(40,200,45);

clear carreauModelSimArray;

fprintf('Fitting Carreau-1972 to synthetic data:\n')
fprintf('Fixing (mu0,lambda) pairs, fitting n, calculating fval ... ')
for j=1:length(mu0)    
    for k=1:length(lambda)
        [n,fit,fval]=LogFitCarreau_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),gamSynth,muSynth,bl,bu,x0,0);
        carreauModelSimArray.n(j,k)=n;
        carreauModelSimArray.fit{j,k}=fit;
        carreauModelSimArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 3b
subplot(2,2,3)
contour(lambda,mu0,carreauModelSimArray.fval,100);colorbar;
xlabel('\(\lambda\)','interpreter','latex');
ylabel('\(\mu_0\)','interpreter','latex');
title('\(\ell(\mu_0,\lambda,\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');

%% Carreau-1972 log fnval fit for synth data > 1e-2 shear
bl = 0.001;
bu = 1;
x0 = 0.44;

mu0=linspace(50,350,40);
lambda=linspace(40,200,45);

clear carreauModelSimArray;

fprintf('Fitting Carreau-1972 to restricted synthetic data:\n')
fprintf('Fixing (mu0,lambda) pairs, fitting n, calculating fval ... ')
for j=1:length(mu0)
    for k=1:length(lambda)
        [n,fit,fval]=LogFitCarreau_Fixed_mu0_lambda(mu0(j), ...
            lambda(k),gamSynth(gamSynth > 1e-2),muSynth(gamSynth > 1e-2),bl,bu,x0,0);
        carreauModelSimArray.n(j,k)=n;
        carreauModelSimArray.fit{j,k}=fit;
        carreauModelSimArray.fval(j,k)=fval;
    end
end
fprintf('done\n')

%% Figure 3c
subplot(2,2,4)
contour(lambda,mu0,carreauModelSimArray.fval,100);colorbar;
xlabel('\(\lambda\)','interpreter','latex');
ylabel('\(\mu_0\)','interpreter','latex');
title('\(\ell(\mu_0,\lambda,\diamond)\)','interpreter','latex');
box on;
set(gca,'tickdir','out');

%%
print(3,'-dpdf','figure3.pdf')