%% Casson
% Bounds
%     tau0, muPl
bl = [0,    0.1];
bu = [20,  20];
x0 = [5,  5];

shrData = SkalakData(:,1);
viscData = SkalakData(:,2);

%solve
[cassonModel.tau0,cassonModel.muPL, ...
    cassonModel.fit,cassonModel.fval,cassonModel.hessian] = ...
    LogFitCasson(shrData,viscData,bl,bu,x0);

cassonModel
%%
wd=10;hh=6.5;
figure(1);clf;
plot(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),400);
plot(gam,cassonModel.fit(gam),'b');
%plot(gam,Carreau1972(params_alt,gam),'r--');
hl=legend('Skalak data','best fit','alternative fit');
set(hl,'box','off');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
ylim([1,100])
box on;
set(gca,'tickdir','out');
set(1,'paperunits','centimeters');
set(1,'papersize',[wd hh]);
set(1,'paperposition',[0 0 wd hh]);
%%
wd=10;hh=6.5;
figure(2);clf;
loglog(SkalakData(:,1),SkalakData(:,2),'ko');
hold on;
gam=linspace(SkalakData(1,1),SkalakData(end,1),400);
loglog(gam,cassonModel.fit(gam),'b');
%plot(gam,Carreau1972(params_alt,gam),'r--');
hl=legend('Skalak data','best fit','alternative fit');
set(hl,'box','off');
xlabel('\(\dot{\gamma}\) (s\(^{-1}\))','interpreter','latex');
ylabel('\(\mu(\dot{\gamma})\) (cP)','interpreter','latex');
ylim([1,100])
box on;
set(gca,'tickdir','out');
set(2,'paperunits','centimeters');
set(2,'papersize',[wd hh]);
set(2,'paperposition',[0 0 wd hh]);


%% Casson, Fix each (tau0,muPL) in turn and find optimal n and associated fnval
% n
tau0=linspace(1,10,200);
muPL=linspace(1,10,200);

clear cassonModelArray;
costFunc = @(c) sum( (log(sqrt(c(1)./shrData) + sqrt(c(2))) ...
    - log(sqrt(viscData))).^2);
for j=1:length(tau0)
    ['j = ' num2str(j) ' / ' num2str(length(tau0))]
    for k=1:length(muPL)        
        cassonModelArray.fval(j,k)=costFunc([tau0(j),muPL(k)]);
    end
end

figure(3);clf;hold on;
hc=contour(muPL,tau0,cassonModelArray.fval,100);colorbar;
[T0,MPL] = meshgrid(tau0,muPL);
% hc=surf(T0,MPL,cassonModelArray.fval);colorbar;
shading interp
xlabel('muPL')
ylabel('tau_0')
hold on
plot(cassonModel.muPL,cassonModel.tau0,'b*');
